$(document).ready(function(){
    
    var hint = $("<div id='hint'><h4>Welcome!<h4></div>");
   $("body").append(hint);

   $( "h3" ).each(function( i ) {
       var li; 
       $(this).attr("id",i+1);
       li="<li><a href=#"+$(this).attr("id")+">"+$(this).text()+"</li>";
       $("ul").append(li);
   });
  
  var left;
  var top;
       
   $("#h2").on('mousemove mouseover', function(e){
       left = e.pageX+30;
       top = e.pageY;
       hint.removeClass("hide");
       hint.css("display", "block");
       hint.css({"left":left, "top":top});
   }).mouseleave(function(){
       hint.css("display", "none");
   });
});